﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LannisterCarriage
{
    class Program
    {
        /// <summary>
        /// Calculates and shows the total distance for a given route
        /// </summary>
        /// <param name="args">Arguments received by the application</param>
        /// <param name="routes">Array with all the available routes</param>
        private static void Distance(string[] args, string[] routes)
        {
            #region Check for the number of arguments expected
            if (args.Length != 3)
                throw new Exception("Invalid number of arguments! Expected 3, found " + args.Length.ToString());
            #endregion

            #region Variables declaration and starting values
            string route = args[2].Replace("-", "").ToUpper();//Requested route
            int distance=0;
            bool done = false;
            int index = 0;
            #endregion

            while (!done)
            {
                string temp = route.Substring(index, 2);

                int i = 0;
                while (!routes[i].StartsWith(temp))
                {
                    if (i == routes.Length - 1)
                        throw new Exception("No Such Route as: " + args[2]);
                    i++;
                }
                distance = distance + Convert.ToInt16(routes[i].Substring(2, 1));
                
                if (route.EndsWith(temp))
                    done = true;
                else index++;
            }

            Console.Write("Distance of route " + args[2] + ": " + distance.ToString());

        }
        /// <summary>
        /// Shows the available routes between two given cities with a maximum number of stops
        /// </summary>
        /// <param name="args">Arguments received by the application</param>
        /// <param name="routes">Array with all the available routes</param>
        private static void AvailableRoutes(string[] args, string[] routes)
        {
            #region Check for the number of arguments expected
            if (args.Length != 5)
                throw new Exception("Invalid number of arguments! Expected 5, found " + args.Length.ToString());
            #endregion

            string starting = args[2].ToUpper(); //Starting town
            string ending = args[3].ToUpper(); //Ending town
            int stops = Convert.ToInt16(args[4]); //Maximum number of stops

        }

        /// <summary>
        /// Calculates and shows the shortest possible route between two given cities
        /// </summary>
        /// <param name="args">Arguments received by the application</param>
        /// <param name="routes">Array with all the available routes</param>
        private static void ShortestRoute(string[] args, string[] routes)
        {
            #region Check for the number of arguments expected
            if (args.Length != 4)
                throw new Exception("Invalid number of arguments! Expected 4, found "+args.Length.ToString());
            #endregion

            string starting = args[2].ToUpper(); //Starting town
            string ending = args[3].ToUpper(); //Ending town

        }

        static void Main(string[] args)
        {
            string filepath = "";
            string function = "";
            try
            {
                if (args.Length > 2)
                {
                    filepath = args[0].Trim(); //The first argument is the filepath to the route file
                    function = args[1].ToLower();//The second argument is the function name
                }
                else throw new Exception("Not enough arguments");

                {
                    String[] routes; //Array of strings in which the routes will be stored
                    #region Reads and Stores the Route File
                    using (StreamReader routefile = new StreamReader(filepath))
                    {
                        String line = routefile.ReadLine();
                        routes = line.Replace(" ","").Split(',');
                        if (routes.Length == 0)
                            throw new Exception("No routes found on the file");
                    }
                    #endregion

                    switch (function)
                    {
                        case "distance": //calculates the total distance of a route
                            Distance(args,routes);
                            break;
                        case "availableroutes": //shows available routes between two cities with a number of stops equal to or less than requested
                            AvailableRoutes(args,routes);
                            break;
                        case "shortestroute": //calculates the shortest route between two cities
                            ShortestRoute(args,routes);
                            break;
                        default:
                            throw new Exception("Unrecognized function: " + function);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
