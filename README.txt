To compile and run the application the following will be required:
	.NET Framework 4.5.1 RC or newer
	.Microsoft Visual Studio 2010 SP1 or Microsft Visual C# 2010 Express or newer

Use Instructions:

A route file must be provided in plain text with a single line containing the available routes as per the example: AB3, BC6, CD2, DA5
A function must he chosen for the application by entering one of the following: distance, availableroutes(not fully implemented), shortestroute(not fully implemented)
Addition parameters must be passed to the program depending on the selected function:
	.A route must be passed in the following format for the distance function: A-B-C-D
	.The starting and ending city, followed by the maximum number of stops must be passed in the following format for the availableroutes function: starting ending maximumstops -> A A 5
	.The starting and ending city must be passed in the following format for the shortestroute function: starting ending -> A D

some examples of a correct command lines are as follows:

lannistercarriage routes.txt distance A-C-D
lannistercarriage routes.txt availableroutes A E 3
lannistercarriage routes.txt shortestroute A D